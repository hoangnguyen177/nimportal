'use strict';

angular.module('strudelWeb.resource-manager', ['ngRoute', 'ngResource'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/resource-manager', {
            templateUrl: 'resource-manager/resource-manager.html',
            controller: 'ResourceManagerCtrl'
        });
    }])

    .controller('ResourceManagerCtrl', ['$scope', '$rootScope', '$resource', '$interval', '$location', '$mdSidenav', '$mdMedia', '$mdDialog', 'settings',
        function ($scope, $rootScope, $resource, $interval, $location, $mdSidenav, $mdMedia, $mdDialog, settings) {
            // Resources
            var configurationResource = $resource(settings.URLs.apiBase + settings.URLs.configList);
            var sessionInfoResource = $resource(settings.URLs.apiBase + settings.URLs.sessionInfo);
            //var endSessionResource = $resource(settings.URLs.apiBase + settings.URLs.logout);
            var listResourcesResource = $resource(settings.URLs.apiBase + settings.URLs.nimrodGetResources , {}, {
              'get': {
                  isArray: true
              }
            });
               
            var deleteResourceResource = $resource(settings.URLs.apiBase + settings.URLs.nimrodDeleteResource, {}, {
                'get': {
                    isArray: true
                }
            });

            document.getElementById("myCarousel").style.display="none";
            document.getElementById("login").style.display="none";
            document.getElementById("logout-btn").style.display="block";
            document.getElementById("expmgr").style.display="block";
            document.getElementById("resourcemgr").style.display="block";
             
            $scope.gridOptions = {
                enableRowSelection: true,
                enableRowHeaderSelection: false,
                enableSelectAll: false,
		multiSelect: false,
                rowHeight: 35,
                showGridFooter:false,
                paginationPageSizes: [2, 5, 10],
                paginationPageSize: 2,
		data: [],
            };

            $scope.gridOptions.columnDefs = [
              { name: 'info', displayName: 'Resource Info', allowCellFocus : false },
              { name: 'type', displayName: 'Type', allowCellFocus : false},
              { name: 'joblimit', displayName: 'Job Limit', allowCellFocus : false },
              { name: 'arch', displayName: 'Architecture', allowCellFocus : false },
              { name: 'maxtime', displayName: 'Max Time', allowCellFocus : false}
            ];

            $scope.gridOptions.onRegisterApi = function( gridApi ) {
                $scope.gridApi = gridApi;
            };
                  
            //refresh resource
            var resListRefreshInProgress = false;
            var resListRefreshPromise;
            $scope.refreshResCountdown = 10;
            $scope.refreshResourceList = function () {
                function doRefresh() {
                    resListRefreshInProgress = true;
                    listResourcesResource.get({}
                        //'configuration': configuration.configuration.fullName,
                        //'username': userName
                    ).$promise.then(
                        function (data) {
                            if (data.length === 0 || $scope.gridOptions.data.length === 0 ) {
                                $scope.gridOptions.data = data;
                                return;
                            }
                            //this is n^2 :(
                            for(var i=0; i < data.length; i++){
                                var found = false;
                                for(var j=0; j< $scope.gridOptions.data.length; j++){
                                    if($scope.gridOptions.data[j]['info'] === data[i]['info'] &&
                                            $scope.gridOptions.data[j]['type'] === data[i]['type']){
                                        //update
                                        $scope.gridOptions.data[j]['ftime']=data[i]['ftime'];
                                        $scope.gridOptions.data[j]['cost']=data[i]['cost'];
                                        $scope.gridOptions.data[j]['myproxy']=data[i]['myproxy'];
                                        $scope.gridOptions.data[j]['wtime']=data[i]['wtime'];
                                        $scope.gridOptions.data[j]['maxtime']=data[i]['maxtime'];
                                        $scope.gridOptions.data[j]['joblimit']=data[i]['joblimit'];
                                        $scope.gridOptions.data[j]['proxy']=data[i]['proxy'];
                                        $scope.gridOptions.data[j]['path']=data[i]['path'];
                                        $scope.gridOptions.data[j]['fsize']=data[i]['fsize'];
                                        $scope.gridOptions.data[j]['immediate']=data[i]['immediate'];
                                        $scope.gridOptions.data[j]['compid']=data[i]['compid'];
                                        $scope.gridOptions.data[j]['ctime']=data[i]['ctime'];
                                        $scope.gridOptions.data[j]['idletime']=data[i]['idletime'];
                                        $scope.gridOptions.data[j]['arch']=data[i]['arch'];
                                        $scope.gridOptions.data[j]['queue']=data[i]['queue'];
                                        found = true;
                                    }
                                }
                                //add item
                                if(found === false){
                                    $scope.gridOptions.data.push(data[i]);
                                }
                            }

                            for(var i=0; i < $scope.gridOptions.data.length; i++){
                                var found = false;
                                for(var j=0; j< data.length; j++){
                                    if($scope.gridOptions.data[i]['info'] === data[j]['info'] && 
                                            $scope.gridOptions.data[i]['type'] === data[j]['type']){
                                        found = true;
                                    }
                                }
                                if(found === false){
                                    $scope.gridOptions.data.splice(i, 1);
                                }
                            }
                        },
                        function (error) {
                            $rootScope.$broadcast("notify", "Could not refresh resource list!");
                        }
                    ).finally(
                        function () {
                            resListRefreshInProgress = false;
                            resListRefreshPromise = $interval(function () {
                                if ($scope.refreshResCountdown > 0) {
                                    $scope.refreshResCountdown--;
                                }
				else{
				    $scope.refreshResCountdown = 10;
				    doRefresh();
				}
                            }, 5000);
                            //resListRefreshPromise.then(function () {
                            //    doRefresh();
                            //});

                        }
                    );
                }

                if (angular.isDefined(resListRefreshPromise) && !resListRefreshInProgress) {
                    $interval.cancel(resListRefreshPromise);
                    resListRefreshPromise = undefined;
                }
                if (!resListRefreshInProgress) {
                    doRefresh();
                }
            };

            // Stop refreshing the experiments if the route changes
            $scope.$on('$destroy', function () {
                if (resListRefreshPromise) {
                    $interval.cancel(resListRefreshPromise);
                }
            });
            //list resource is in progress
            $scope.resourceListRefreshInProgress = function(){
                return resListRefreshInProgress;
            }
            
            $scope.removeResource = function (event, info, type) {
                var dialog = $mdDialog.confirm()
                    .title("Are you sure?")
                    .content("Are you sure you would like to delete resource " + info + "?")
                    .targetEvent(event)
                    .ok("Yes")
                    .cancel("No");
                $mdDialog.show(dialog).then(function () {
                    doDeleteResource(info, type);
                });
              };
    
              var doDeleteResource = function (info, type) {
              $rootScope.$broadcast("notify", "Deleting resource " + info + "...");
              stopExpResource.get({
                  'resource': info,
                  'restype': type,
                  //'configuration': configuration.configuration.fullName
              }).$promise.then(
                    function (data) {
                        $rootScope.$broadcast("notify", "Resource deleted!");
                    },
                    function (error) {
                        $rootScope.$broadcast("notify", "Could not delete resource!");
                    }
                );
              };

            var getProjectsResource = $resource(settings.URLs.apiBase + settings.URLs.getProjects, {}, {
                'get': {
                    isArray: true
                }
            });
            
    	    
            // Performs any initialisation after configurations
            // have been fetched from the server.
            $scope.configuration = {};
            var bootstrap = function (configuration) {
                $scope.configuration = configuration;
            };
            
            // Sets the selection as selected, and sets new experiment parameters
            // to their defaults.
            $scope.setSelectedConfiguration = function (name, configuration) {
                if (angular.isDefined($scope.selectedConfiguration) && $scope.selectedConfiguration.name === name) {
                    return;
                }
                document.getElementById("batch-btn").style.background="black";
                $scope.selectedConfiguration = {
                    name: name,
                    configuration: configuration
                };
            };

            // For a given authBackendName, filter all accessible configurations
            var getAccessibleConfigurations = function (authBackendName, configurationData) {
                var configs = {};
                for (var i in configurationData) {
                    if (configurationData.hasOwnProperty(i) && configurationData[i].hasOwnProperty("authBackendNames")) {
                        var c = configurationData[i];
                        if (c.authBackendNames.indexOf(authBackendName) > -1) {
                            var facilityAndFlavour = i.split("|");
                            if (!configs.hasOwnProperty(facilityAndFlavour[0])) {
                                configs[facilityAndFlavour[0]] = {};
                            }
                            c.configurations.fullName = i;

                            // Get available projects (if any) for a configuration
                            (function(config) {
                                // Gets a list of projects if supported by backend
                                getProjectsResource.get({
                                    'configuration': config.fullName
                                }).$promise.then(function (projects) {
                                    config.projects = [];
                                    for (var i = 0; i < projects.length; i++) {
                                        var project = projects[i].group;
                                        if (project !== "") {
                                            config.projects.push(project);
                                        }
                                    }
                                });
                            })(c.configurations);

                            configs[facilityAndFlavour[0]][facilityAndFlavour[1]] = c.configurations;
                        }
                    }
                }
                return configs;
            };

            // Gets the session data and redirects to the login screen if the user is not logged in
            sessionInfoResource.get({}).$promise.then(function (sessionData) {
                if (sessionData.has_certificate !== "true") {
                    $location.path("/system-selector");
                    return;
                }
                $scope.session = sessionData;
                configurationResource.get({}).$promise.then(function (configurationData) {
                    bootstrap(getAccessibleConfigurations(sessionData.auth_backend_name, configurationData));
                    $scope.refreshResourceList();
                })
            }); 
            // Signs out the current user and redirects to the login screen
//            $scope.doSignout = function () {
//                endSessionResource.get({}, function () {
//                    //$location.path("/system-selector");
//                    if(document.getElementById("login").style.display="none")
//                    {
//                        document.getElementById("login").style.display="block";
//                        document.getElementById("register").style.display="block";
//                    }
//
//                    $location.path("/tern-landingpage");
//                });
//            };

        }]);
