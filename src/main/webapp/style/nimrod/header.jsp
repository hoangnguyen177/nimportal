<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Nimrod Portal</title>

    <!-- Bootstrap Core CSS -->
    <link href="style/nimrod/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="style/nimrod/modern-business.css" rel="stylesheet">
    <link href="style/nimrod/nimrod-style.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="bower_components/angular-material/angular-material.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded',
                function () {
                    $.feedback({
                        ajaxURL: '/strudel-web/api/feedback/',
                        html2canvasURL: 'feedback/development/html2canvas.js',
                        onClose: function() {
                            //window.location.reload();
                        }
                    });
                }, false);
    </script>

<!--
    <script type="text/javascript">
        window.setTimeout(function(){
                var ele=document.querySelector("#atlwdg-trigger");
                ele.style.backgroundColor="#BF6000";
                ele.style.fontSize="medium";
        //      $('#atlwdg-trigger').css({'background':'#BF6000','width':'80px','height':'25px','font-size':'medium'});
        },500);

    </script>
-->
    <script type="text/javascript" src="https://ternaus.atlassian.net/s/d41d8cd98f00b204e9800998ecf8427e-T/-fayin4/100018/c/1000.0.11/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=en-UK&collectorId=e045390d"></script>

</head>
<body ng-controller="AppCtrl">



    <div class="container clearfix" ng-hide="toolbarHidden">
        <div class="logo">
            <a href="https://rcc.uq.edu.au" rel="nofollow">
                <img alt="RCC" src="images/rcc-logo.jpg"/>
            </a>
        </div>
        <div class="banner-text">
            Nimrod Portal
        </div>
    </div>

    <!-- Navigation -->
    <nav class="navbar main-menu" role="navigation" ng-hide="toolbarHidden">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

               <!-- <a class="navbar-brand" href="index.html">Collaborative Environment for Scholarly Research and Analysis</a>-->
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#"><i class="fa fa-home fa-fw" aria-hidden="true"></i>&nbsp; Home</a>
                    </li>
                    <li>
                        <a href="#/about">About</a>
                    </li>
                    <li>
                        <a href="#/contact">Contact</a>
                    </li>
                    <li><a href="#/citation">Citation</a></li>
                    <li><a href="#/faq">FAQ</a></li>
                    <li><a href="#/accesspolicy">Policy</a></li>
                    <li><a id="login" href="#/system-selector">Login</a></li>
                    <li><a id="expmgr" href="#/experiment-manager">Experiments</a></li>
                    <li><a id="resourcemgr" href="#/resource-manager">Resources</a></li>
                    <li><a id="logout-btn" ng-click="doSignout()">Logout</a></li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
