'use strict';

describe('strudelWeb.experiment-manager module', function() {

  beforeEach(module('strudelWeb.experiment-manager'));

  describe('experiment-manager controller', function(){

    it('should ....', inject(function($controller) {
      //spec body
      var experimentManagerCtrl = $controller('ExperimentManagerCtrl');
      expect(experimentManagerCtrl).toBeDefined();
    }));

  });
});