'use strict';

angular.module('strudelWeb.experiment-manager', ['ngRoute', 'ngResource', 'ui.grid', 'ui.grid.expandable', 'ui.grid.selection', 'ui.grid.pinning'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/experiment-manager', {
            templateUrl: 'experiment-manager/experiment-manager.html',
            controller: 'ExperimentManagerCtrl'
        });
    }])

    .controller('ExperimentManagerCtrl', ['$scope', '$rootScope', '$resource', '$interval', '$location', '$mdSidenav', '$mdMedia', '$mdDialog', 'settings',
        function ($scope, $rootScope, $resource, $interval, $location, $mdSidenav, $mdMedia, $mdDialog, settings) {
            // Resources
            var configurationResource = $resource(settings.URLs.apiBase + settings.URLs.configList);
            var sessionInfoResource = $resource(settings.URLs.apiBase + settings.URLs.sessionInfo);
            //var endSessionResource = $resource(settings.URLs.apiBase + settings.URLs.logout);

            var getProjectsResource = $resource(settings.URLs.apiBase + settings.URLs.getProjects, {}, {
                'get': {
                    isArray: true
                }
            });

            var listExperimentsResource = $resource(settings.URLs.apiBase + settings.URLs.nimrodGetExperiments , {}, {
              'get': {
                  isArray: true
              }
            });
            
            var startExpResource = $resource(settings.URLs.apiBase + settings.URLs.nimrodStartExperiment, {}, {
              'get': {
                  isArray: true
              }
            });
            
            var stopExpResource = $resource(settings.URLs.apiBase + settings.URLs.nimrodStopExperiment, {}, {
              'get': {
                  isArray: true
              }
            });

            var deleteExpResource = $resource(settings.URLs.apiBase + settings.URLs.nimrodDeleteExperiment, {}, {
              'get': {
                  isArray: true
              }
            });
            
            var addServerResource = $resource(settings.URLs.apiBase + settings.URLs.nimrodAddServer, {}, {
              'get': {
                  isArray: true
              }
            });
            
            var removeServerResource = $resource(settings.URLs.apiBase + settings.URLs.nimrodRemoveServer, {}, {
              'get': {
                  isArray: true
              }
            });
            
    	    document.getElementById("myCarousel").style.display="none";
    	    document.getElementById("login").style.display="none";
    	    document.getElementById("logout-btn").style.display="block";
    	    document.getElementById("expmgr").style.display="block";
    	    document.getElementById("resourcemgr").style.display="block";


             //grid
            $scope.gridOptions = {
                enableRowSelection: true,
                enableRowHeaderSelection: false,
                enableSelectAll: false,
		multiSelect: false,
		noUnselect: true,
                rowHeight: 30,
                showGridFooter:false,
                paginationPageSizes: [6, 12, 24],
                paginationPageSize: 6,
		data: []
	   };
            
            $scope.gridOptions.columnDefs = [
                { name: 'label', displayName: 'Label', allowCellFocus : false },
                { name: 'type', displayName: 'Type', allowCellFocus : false},
                { name: 'status', displayName: 'Status', allowCellFocus : false }
            ];
           
            //$scope.gridOptions.multiSelect = false;
            //$scope.gridOptions.modifierKeysToMultiSelect = false;
            //$scope.gridOptions.noUnselect = true;
            $scope.gridOptions.onRegisterApi = function( gridApi ) {
              $scope.gridApi = gridApi;
            };
 
            // Performs any initialisation after configurations
            // have been fetched from the server.
            $scope.configuration = {};
            var bootstrap = function (configuration) {
                $scope.configuration = configuration;
            };
            
            // Sets the selection as selected, and sets new experiment parameters
            // to their defaults.
            $scope.setSelectedConfiguration = function (name, configuration) {
                if (angular.isDefined($scope.selectedConfiguration) && $scope.selectedConfiguration.name === name) {
                    return;
                }
                document.getElementById("batch-btn").style.background="black";
                $scope.selectedConfiguration = {
                    name: name,
                    configuration: configuration
                };
            };

            // For a given authBackendName, filter all accessible configurations
            var getAccessibleConfigurations = function (authBackendName, configurationData) {
                var configs = {};
                for (var i in configurationData) {
                    if (configurationData.hasOwnProperty(i) && configurationData[i].hasOwnProperty("authBackendNames")) {
                        var c = configurationData[i];
                        if (c.authBackendNames.indexOf(authBackendName) > -1) {
                            var facilityAndFlavour = i.split("|");
                            if (!configs.hasOwnProperty(facilityAndFlavour[0])) {
                                configs[facilityAndFlavour[0]] = {};
                            }
                            c.configurations.fullName = i;

                            // Get available projects (if any) for a configuration
                            (function(config) {
                                // Gets a list of projects if supported by backend
                                getProjectsResource.get({
                                    'configuration': config.fullName
                                }).$promise.then(function (projects) {
                                    config.projects = [];
                                    for (var i = 0; i < projects.length; i++) {
                                        var project = projects[i].group;
                                        if (project !== "") {
                                            config.projects.push(project);
                                        }
                                    }
                                });
                            })(c.configurations);

                            configs[facilityAndFlavour[0]][facilityAndFlavour[1]] = c.configurations;
                        }
                    }
                }

                return configs;
            };

            // Gets the session data and redirects to the login screen if the user is not logged in
            sessionInfoResource.get({}).$promise.then(function (sessionData) {
                if (sessionData.has_certificate !== "true") {
                    $location.path("/system-selector");
                    return;
                }
                $scope.session = sessionData;
                configurationResource.get({}).$promise.then(function (configurationData) {
                    bootstrap(getAccessibleConfigurations(sessionData.auth_backend_name, configurationData));
                })
            });
            
             //refresh experiment
            var expListRefreshInProgress = false;
            var expListRefreshPromise;
            $scope.refreshCountdown = 10;
            $scope.refreshExpList = function () {
                function doRefresh() {
                    expListRefreshInProgress = true;
                    listExperimentsResource.get({
                        //'configuration': configuration.configuration.fullName,
                        //'username': userName
                    }).$promise.then(
                        function(data){
                                if (data.length === 0 || $scope.gridOptions.data.length === 0) {
				    $scope.gridOptions.data = data;
				    console.log($scope.gridOptions.data);	
                                    return;
                                }
                                //this is n^2 :(
                                for(var i=0; i < data.length; i++){
                                    var found = false;
                                    for(var j=0; j< $scope.gridOptions.data.length; j++){
                                        if($scope.gridOptions.data[j]['label'] === data[i]['label'] && 
                                                $scope.gridOptions.data[j]['type'] === data[i]['type']){
                                            //update
                                            $scope.gridOptions.data[j]['status']=data[i]['status'];
                                            $scope.gridOptions.data[j]['active']=data[i]['active'];
                                            $scope.gridOptions.data[j]['done']=data[i]['done'];
                                            $scope.gridOptions.data[j]['fail']=data[i]['fail'];
                                            $scope.gridOptions.data[j]['ready']=data[i]['ready'];
                                            $scope.gridOptions.data[j]['ftime']=data[i]['ftime'];
                                            $scope.gridOptions.data[j]['fsize']=data[i]['fsize'];
                                            $scope.gridOptions.data[j]['totaltime']=data[i]['totaltime'];
                                            $scope.gridOptions.data[j]['wtime']=data[i]['wtime'];
                                            $scope.gridOptions.data[j]['ctime']=data[i]['ctime'];
                                            found = true;
                                        }
                                    }
                                    //add item
                                    if(found === false){
                                        $scope.gridOptions.data.push(data[i]);
                                    }
                                }
    
                                for(var i=0; i < $scope.gridOptions.data.length; i++){
                                    var found = false;
                                    for(var j=0; j< data.length; j++){
                                        if($scope.gridOptions.data[i]['label'] === data[j]['label'] && 
                                                $scope.gridOptions.data[i]['type'] === data[j]['type']){
                                            found = true;
                                        }
                                    }
                                    if(found === false){
                                        $scope.gridOptions.data.splice(i, 1);
                                    }
                                }//end for
                            
                        }//end function data
                        ,
                        function (error) {
                            $rootScope.$broadcast("notify", "Could not refresh experiment list!");
                        }
                    )
		    .finally(
                        function () {
                            expListRefreshInProgress = false;
                            expListRefreshPromise = $interval(function () {
				if ($scope.refreshCountdown > 0) {
                                    $scope.refreshCountdown--;
                                }
				else{
				    $scope.refreshCountdown = 10;
				    doRefresh();
				}
                            }, 3000);
                            //expListRefreshPromise.then(function () {
                            //    doRefresh();
                            //});
                        }
                    );
                }

                if (angular.isDefined(expListRefreshPromise) && !expListRefreshInProgress) {
                    $interval.cancel(expListRefreshPromise);
                    expListRefreshPromise = undefined;
                }
                if (!expListRefreshInProgress) {
                    doRefresh();
                }
            };

            // Stop refreshing the experiments if the route changes
            $scope.$on('$destroy', function () {
                if (expListRefreshPromise) {
                    $interval.cancel(expListRefreshPromise);
                }
            });
            //list experiment is in progress
            $scope.expListRefreshInProgress = function(){
                return expListRefreshInProgress;
            }
	    $scope.refreshExpList();
            // Signs out the current user and redirects to the login screen
//            $scope.doSignout = function () {
//                endSessionResource.get({}, function () {
//                    //$location.path("/system-selector");
//                    if(document.getElementById("login").style.display="none")
//                    {
//                        document.getElementById("login").style.display="block";
//                        document.getElementById("register").style.display="block";
//                    }
//
//                    $location.path("/tern-landingpage");
//                });
//            };

        }]);
