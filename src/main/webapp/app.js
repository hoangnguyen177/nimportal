'use strict';

// Declare app level module which depends on views, and components
angular.module('strudelWeb', [
    'ngMaterial',
    'ngRoute',
    'ui.grid', 
    'ui.grid.selection',
    'strudelWeb.system-selector',
    'strudelWeb.experiment-manager',
//    'strudelWeb.experiment-viewer',
    'strudelWeb.resource-manager',
//    'strudelWeb.resource-viewer',
    'strudelWeb.version',
    'strudelWeb.landingpage',
    'strudelWeb.partials',
    'strudelWeb.faqdirectives'
]).
    config(['$routeProvider', '$httpProvider',
        function ($routeProvider, $httpProvider) {
            $routeProvider.otherwise({redirectTo: '/landingpage'});
            $httpProvider.interceptors.push('APIInterceptor');
        }])
    .constant('settings', {
        'URLs': {
            'base': '/nimrod/',
            'apiBase': '/nimrod/api/',
            'configList': 'configurations',
            'oauthStart': 'login',
            'logout': 'end_session',
            'sessionInfo': 'session_info',
            'registerKey': 'register_key',
            'getProjects': 'execute/getprojects',
            
            'nimrodGetExperiments': 'execute/getexperiments',
            'nimrodGetResources': 'execute/getresources',
            'nimrodGetResourcesInExp': 'execute/getresourcesinexp',
            'nimrodStartExperiment': 'execute/startexperiment',
            'nimrodGetErrors': 'execute/geterrors',
            'nimrodGetAgents': 'execute/getagents',
            'nimrodStopExperiment': 'execute/stopexperiment',
            'nimrodDeleteExperiment': 'execute/deleteexperiment',
            'nimrodDeleteResource': 'execute/deleteresource',
            'nimrodAddServer': 'execute/addserver',
            'nimrodRemoveServer': 'execute/removeserver',
            'nimrodAddrun': 'execute/addrun',
            'nimrodGenerate': 'execute/generate',
            'nimrodCreate': 'execute/create',
            'nimrodAddResource': 'execute/addresource',
            'nimrodMoveFilesToExperiment': 'execute/movefilestoexperiment',
            'nimrodDeleteFileFromExperiment': 'execute/deletefilefromexperiment',
            'nimrodInitNimrod': 'execute/initnimrod',
            
            'messages': 'messages'
        },
    })
    .service('APIInterceptor', ['$rootScope', '$location', '$injector', '$timeout', '$log', 'settings',
        function ($rootScope, $location, $injector, $timeout, $log, settings) {
            var service = this;

            // Keep track of the failures per URL
            var failures = {};

            service.request = function (config) {
                return config;
            };

            service.response = function (response) {
                if (response.status !== 500 && failures.hasOwnProperty(response.config.url)) {
                    delete failures[response.config.url];
                }
                return response;
            };

            service.responseError = function (response) {
                // Redirect to login page on unauthorised response
                if (response.status === 403) {
                    $rootScope.$broadcast("notify", "You've been logged out!");
                    $location.path('/');

                // Retry on 500
                } else if (response.status === 500) {
                    var url = response.config.url;
                    if (failures.hasOwnProperty(url)) {
                        failures[url]++;
                    } else {
                        failures[url] = 1;
                    }
                    if (failures[url] < settings.maxRetryOnServerError) {
                        $log.error("Retrying failed request (500) for URL " + url + " (" + failures[url] + "failures so far)");
                        return $timeout(function () {
                            var $http = $injector.get('$http');
                            return $http(response.config);
                        }, 3000);
                    }
                }
                return response;
            };
        }])
    .controller('AppCtrl', ['$mdToast', '$rootScope', '$scope', 'settings','$location','$resource',function ($mdToast, $rootScope, $scope,settings,$location,$resource) {
        $rootScope.$on('notify', function (event, message) {
            console.log(message);
            $mdToast.show(
                $mdToast.simple()
                    .content(message)
                    .position("bottom right")
                    .hideDelay(10000)
            );
        });
        document.getElementById("logout-btn").style.display="none"; 
        document.getElementById("expmgr").style.display="none"; 
        document.getElementById("resourcemgr").style.display="none"; 
        var hideFeedbackButton = function() {
            var buttonElements = document.getElementsByClassName("feedback-btn");
            if (buttonElements.length > 0) {
                buttonElements[0].style.display = 'none';
            }
        };
        var showFeedbackButton = function() {
            var buttonElements = document.getElementsByClassName("feedback-btn");
            if (buttonElements.length > 0) {
                //hide original cvl feedback button, ys
        		//buttonElements[0].style.display = 'block';
        		buttonElements[0].style.display='none';
            }
        };
        $scope.toolbarHidden = false;
        $rootScope.$on('makeToolbarVisible', function (event) {
            $scope.toolbarHidden = false;
            showFeedbackButton();
        });

        $rootScope.$on('makeToolbarInvisible', function (event) {
            $scope.toolbarHidden = true;
            hideFeedbackButton();
        });
        $rootScope.$on('$routeChangeSuccess', function (event) {
            $scope.toolbarHidden = false;
            showFeedbackButton();
        });
        
        var endSessionResource = $resource(settings.URLs.apiBase + settings.URLs.logout);
            $scope.doSignout = function () {
                endSessionResource.get({}, function () {
                    if(document.getElementById("login").style.display="none")
                    {
                        document.getElementById("login").style.display="inline-block";
                        document.getElementById("logout-btn").style.display="none"; 
                        document.getElementById("expmgr").style.display="none"; 
                        document.getElementById("resourcemgr").style.display="none"; 
                    }

                    $location.path("/landingpage");
                });
            };        
    }]);
