<!DOCTYPE html>
<html lang="en" ng-app="strudelWeb" class="no-js" ng-cloak>

<jsp:include page="style/nimrod/header.jsp" />
    <!-- Header Carousel -->
    <header id="myCarousel" class="carousel slide fixed-width" ng-hide="toolbarHidden">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active carousel-wordle">
                <img src="images/nimrod/nimrod-all.jpg" alt="One"/>
            </div>
            <div class="item carousel-share">
                <img src="images/nimrod/nimrodg-1.png" alt="Three"/>
            </div>
            <div class="item carousel-share">
                <img src="images/nimrod/nimrodg-2.jpg" alt="Three"/>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </header>

    <div ng-view></div>

<jsp:include page="style/nimrod/footer.jsp" />

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>


    <script src="bower_components/jquery/dist/jquery.js"></script>
    <script src="bower_components/angular/angular.js"></script>
    <script src="bower_components/angular-cookies/angular-cookies.js"></script>
    <script src="bower_components/angular-resource/angular-resource.js"></script>
    <script src="bower_components/angular-aria/angular-aria.js"></script>
    <script src="bower_components/angular-animate/angular-animate.js"></script>
    <script src="bower_components/angular-material/angular-material.js"></script>

    <link rel="stylesheet" href="bower_components/angular-ui-grid/ui-grid.css"></link>
    <script src="bower_components/angular-ui-grid/ui-grid.js"></script>

    <script src="bower_components/angular-route/angular-route.js"></script>
    <script src="feedback/development/feedback.min.js"></script>
    <script src="components/version/version.js"></script>
    <script src="components/version/version-directive.js"></script>
    <script src="components/version/interpolate-filter.js"></script>

    <script src="app.js"></script>
    <script src="landingpage/landingpage.js"></script>
    <script src="partials/partials.js"></script>
    <script src="system-selector/system-selector.js"></script>
    <script src="experiment-manager/experiment-manager.js"></script>
    <!-- script src="experiment-viewer/experiment-viewer.js"></script -->
    <script src="resource-manager/resource-manager.js"></script>
    <!-- script src="resource-viewer/resource-viewer.js"></script -->
    <script src="partials/faqdirective.js"></script>
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

</body>

</html>

